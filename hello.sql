SET SERVEROUTPUT on; 

BEGIN
dbms_output.put_line ('Hello World!');
END;
/


DECLARE
  foo BOOLEAN := TRUE;
  boo VARCHAR(20) := NULL; 
BEGIN

  IF foo = FALSE THEN                     -- Noncompliant
    DBMS_OUTPUT.PUT_LINE('foo = false!');
  ELSIF foo = TRUE THEN                   -- Noncompliant
    DBMS_OUTPUT.PUT_LINE('foo = true!');
  END IF;
END;
/
